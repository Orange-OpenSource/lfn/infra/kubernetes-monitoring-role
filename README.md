Kubernetes Monitoring
============

Ansible role that configure monitoring for infra of a Kubernetes.

Grafana will have some preprovisionned dashboards that are known to be
interested for Kubernetes cluster monitoring.

Requirements
------------

`helm` and `kubectl` with right configuration must be installed on the servers.
`pip` also must be present.

Role Variables
--------------


Dependencies
------------

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: kubernetes-monitoring
```

TODO
----

- [ ] add more dashboards for infrastructure supervision
- [ ] configure kubernetes prometheus service endpoints
- [ ] create some alerts

License
-------

Apache 2.0
